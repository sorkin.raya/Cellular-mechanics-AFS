# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 20:30:23 2016

@author: Guy
"""

from nptdms import TdmsFile
import numpy as np
import re
from scipy import stats
from scipy import optimize
import csv
from matplotlib import pyplot as pp
import os
from collections import defaultdict
from scipy.signal import savgol_filter
import pandas
from statistics import mean, stdev

FORCE_TO_SLOPE = 1


def smooth(x, window_len=11):
    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    s = np.r_[x[window_len - 1:0:-1], x, x[-1:-window_len:-1]]
    w = np.hanning(window_len)
    y = np.convolve(w / w.sum(), s, mode='valid')
    return y


def get_voltage_names(groups):
    for group in groups:
        yield group[group.find('_'):]


def get_minutes(ts):
    return ts.index.values.astype(np.float64) * 1e-10 / 6.0


def get_60hz_time_series(t, y):
    if len(y.shape) > 1:
        is_valid_data = np.isfinite(y[:, 0])
    else:
        is_valid_data = np.isfinite(y)
    y = y[is_valid_data]
    t = t[is_valid_data]
    ts = pandas.DataFrame(data=y, index=t).drop_duplicates().resample('16666U').ffill().dropna()
    return get_minutes(ts), ts.values


def get_jump_idx(x, y, is_jump_positive, fileName='', group=''):
    SLOPE_DIFF_OFFSET = 5
    SMOOTH_WINDOW_SIZE = 5
    if len(y.shape) > 1:  # y[:,0] holds L, and y[:,1] holds the force
        L = y[:, 0]
        force_activation_idx = np.argmax(y[:, 1] > 0.1) - SLOPE_DIFF_OFFSET - SMOOTH_WINDOW_SIZE
        slope_limit = np.max(y[:, 1]) * FORCE_TO_SLOPE  # set slope limit to be the max force applied time a constant
    else:
        L = y
        force_activation_idx = None
        slope_limit = 50
    L = smooth(L, SMOOTH_WINDOW_SIZE)[SMOOTH_WINDOW_SIZE // 2:-SMOOTH_WINDOW_SIZE // 2]
    slope = (L[SLOPE_DIFF_OFFSET:] - L[:-SLOPE_DIFF_OFFSET]) / (x[10] - x[0])
    if not is_jump_positive:
        slope *= -1
    if force_activation_idx:
        max_slope_idx = np.argmax(slope[force_activation_idx:force_activation_idx + 60]) + force_activation_idx
        end_of_slope_idx = np.argmax(slope[max_slope_idx:max_slope_idx + 12] < slope_limit) + max_slope_idx
    else:
        raise Exception('missing force channel, could not determine slope end')

    leftover = x[SLOPE_DIFF_OFFSET // 2 + SMOOTH_WINDOW_SIZE // 2:].shape[0] - slope.shape[0]
    print(x[SLOPE_DIFF_OFFSET // 2 + SMOOTH_WINDOW_SIZE // 2:].shape[0], slope.shape[0])
    fig = pp.figure()
    pp.plot(
        x[SLOPE_DIFF_OFFSET // 2 + SMOOTH_WINDOW_SIZE // 2:-leftover], slope, 'g-',
        [x[max_slope_idx]], [slope[max_slope_idx]], 'ro',
        [x[end_of_slope_idx]], [slope[end_of_slope_idx]], 'bo',
        [x[force_activation_idx], x[force_activation_idx + 60]],
        [slope[force_activation_idx], slope[force_activation_idx + 60]], 'ko',
        [x[max_slope_idx], x[max_slope_idx + 12]], [slope[max_slope_idx], slope[max_slope_idx + 12]], 'k*',
    )
    fig.savefig(fileName + group + '_slope.png')
    pp.close(fig)
    if slope[max_slope_idx] > slope_limit:
        print('%s: found slope: %f' % (is_jump_positive, slope[max_slope_idx]))
        return end_of_slope_idx + SLOPE_DIFF_OFFSET
    print('%s: slope too small: %f' % (is_jump_positive, slope[max_slope_idx]))
    return 0


def forceClampFunction(t, Lcross, tau, LdotV, t0, L0):
    return Lcross * (1 - np.exp(-(t - t0) / tau)) + LdotV * (t - t0) + L0


def getForceClampFit(fileName):
    pp.ioff()
    tdms_file = TdmsFile(fileName)
    forceClampNames = []
    Lcrosses = []
    taus = []
    LdotVs = []
    t0s = []
    L0s = []
    Rs = []
    fitScores = []
    fitScore_stds = []
    Lcrosse_stds = []
    tau_stds = []
    LdotV_stds = []
    t0_stds = []
    L0_stds = []
    R_stds = []
    for group in tdms_file.groups():
        if 'clamp' not in group.lower() and 'clam' not in group.lower():
            continue
        try:
            channel = tdms_file.object(group, 'time (min)')
            t = np.array(channel.data * 60000, dtype='datetime64[ms]')
        except:
            channel = tdms_file.object(group, 'time (s)')
            t = np.array(channel.data * 1000, dtype='datetime64[ms]')
        channel = tdms_file.object(group, 'Distance (um)')
        L = channel.data
        channel = tdms_file.object(group, 'Force (pN)')
        f = channel.data

        # converting to 60hz, scale t to minutes
        t, L = get_60hz_time_series(t, np.stack((L, f)).T)
        # getting end of slope
        estimated_jump_idx = get_jump_idx(t, L, True, fileName=fileName, group=group)
        fitScore_samples = []
        Lcross_samples = []
        tau_samples = []
        LdotV_samples = []
        t0_samples = []
        L0_samples = []
        R_samples = []
        for i in range(-2, 4):
            jump_idx = estimated_jump_idx + i
            t -= t[max(0, jump_idx)]
            t_to_fit = t[jump_idx:]
            L_to_fit = L[jump_idx:, 0]
            LSample = smooth(L[max(0, jump_idx - 70):max(10, jump_idx - 40), 0], 9)[4:-4]
            if len(L) < 10:
                raise Exception('L is too small')
            LBase = np.average(LSample)
            L -= LBase
            LBase = 0
            approx_L0 = L[jump_idx, 0]
            smoothed_L = smooth(L[:, 0], 15)[7:-7]
            range_L0 = abs(smoothed_L[jump_idx + 2] - smoothed_L[max(0, jump_idx - 2)]) / 2.0
            print('L at jump: ', approx_L0)
            print('Looking for L0 around ', approx_L0 - range_L0, ' and ', approx_L0 + range_L0)
            firstIdx = jump_idx  # + 7
            if t[firstIdx] < 0:
                print('t is negative! max t is %f' % max(t))
            try:
                Lvariance = np.var(L[firstIdx:, 0])
                print('fitting curve')
                optimalParams, paramsCovariance = optimize.curve_fit(
                    forceClampFunction, t_to_fit, L_to_fit, [0.2, 0.1, 0.05, 0, approx_L0], None, True, True,
                    ([0.001, 0.001, 0, -0.001, approx_L0 - range_L0], [5, 5, 5, 0.001, approx_L0 + range_L0]))
                print('fit complete, calculating L according to fit')
                Lfit = forceClampFunction(t[firstIdx:], optimalParams[0], optimalParams[1], optimalParams[2],
                                          optimalParams[3], optimalParams[4])
                print('calculating residuals')
                LfitResidualsSquared = (Lfit - L[firstIdx:, 0]) ** 2
                LfitResidualSquaresSum = np.sum(LfitResidualsSquared)
                R = 1 - (LfitResidualSquaresSum / Lvariance)
                LSmoothed = savgol_filter(L[:, 0], 51, 3)
                LSmoothedResidualsSquredSum = np.sum((LSmoothed[firstIdx:] - L[firstIdx:, 0]) ** 2)
                fitScore_samples.append(1.0 / (LfitResidualSquaresSum - LSmoothedResidualsSquredSum))
                Lcross_samples.append(optimalParams[0])
                tau_samples.append(optimalParams[1])
                LdotV_samples.append(optimalParams[2])
                t0_samples.append(optimalParams[3])
                L0_samples.append(optimalParams[4] - LBase)  # was with + L[firstIdx]
                R_samples.append(R)
                print('saving image')
                fig = pp.figure()
                pp.plot(t, L[:, 0], 'g-', t[firstIdx:], Lfit, 'r-')
                fig.savefig(fileName + group + str(i) + '.png')
                pp.close(fig)
                print('image saved')
            except Exception as ex:
                print('faield fitting %s' % ex, group)
        fitScores.append(mean(fitScore_samples))
        forceClampNames.append(group)
        Lcrosses.append(mean(Lcross_samples))
        taus.append(mean(tau_samples))
        LdotVs.append(mean(LdotV_samples))
        t0s.append(mean(t0_samples))
        L0s.append(mean(L0_samples))
        Rs.append(mean(R_samples))

        fitScore_stds.append(stdev(fitScore_samples))
        Lcrosse_stds.append(stdev(Lcross_samples))
        tau_stds.append(stdev(tau_samples))
        LdotV_stds.append(stdev(LdotV_samples))
        t0_stds.append(stdev(t0_samples))
        L0_stds.append(stdev(L0_samples))
        R_stds.append(stdev(R_samples))
        pp.ion()
    return (
        forceClampNames, Lcrosses, taus, LdotVs, t0s, L0s, Rs, fitScores,
        Lcrosse_stds, tau_stds, LdotV_stds, t0_stds, L0_stds, R_stds, fitScore_stds
    )


def writeCSVs(workDir, dictOfNameAndValues, header):
    for k, v in dictOfNameAndValues.items():
        with open(os.path.join(workDir, k + ".csv"), 'w', newline='') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(header)
            for val in v:
                assert (len(header) == len(val))
                csv_out.writerow(val)


def main():
    workDir = r'C:\Data'
    voltage_finder = re.compile(r'(\d+\.?\d*) ?v', re.IGNORECASE)
    clampFits = defaultdict(list)
    for fileName in os.listdir(workDir):
        try:
            if fileName.endswith('.tdms'):
                clampNames, Lcrosses, taus, LdotVs, t0s, L0s, Rs, fitScores, Lcrosse_stds, tau_stds, LdotV_stds, t0_stds, L0_stds, R_stds, fitScore_stds = getForceClampFit(
                    os.path.join(workDir, fileName))

                for i in range(len(clampNames)):
                    clampNames[i] = float(voltage_finder.search(clampNames[i]).group(1))

                batch_name = re.search(r'(data_\d+)', fileName).group(1)
                cell_num = re.search(r'#(\d+)', fileName).group(1)

                for i in range(0, len(clampNames)):
                    clampFits[batch_name].append(
                        (clampNames[i], cell_num, Lcrosses[i], taus[i],
                         LdotVs[i], t0s[i], L0s[i], Rs[i], fitScores[i],
                         Lcrosse_stds[i], tau_stds[i], LdotV_stds[i], t0_stds[i],
                         L0_stds[i], R_stds[i], fitScore_stds[i]
                         ))

        except Exception as ex:
            print('error in file %s: %s' % (fileName, ex))
    print('saving csv for clamp:')
    try:
        writeCSVs(
            workDir, clampFits, [
                'Voltage', 'Cell Num', 'Lcross', 'tau', 'LdotV', 't0', 'L0', 'R^2', 'fit score',
                'Lcross_std', 'tau_std', 'LdotV_std', 't0_std', 'L0_std', 'R^2_std', 'fit score_std'
            ])

    except Exception as e:
        print(e)
    print('done')


if __name__ == "__main__":
    main()


